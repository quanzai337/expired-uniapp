# expired_uniapp

#### 介绍
过期物品提醒微信小程序，可以添加物品、优惠券、各种会员账号、任何带有过期时间的东西都可以
- 前端项目地址：https://gitee.com/quanzai337/expired-uniapp
- 后端项目地址：https://gitee.com/quanzai337/expired_springboot

#### 软件架构
- 1、后端技术栈：springBoot、mybatisPlus、mysql8.0、rabbitMq、redis
- 2、前端技术栈: node.js、uniapp、uView

#### 安装教程
 **后端服务：** 
1.  在application.yml配置文件中更改数据库、redis、rabbitMq、微信小程序平台配置信息
2.  在application-druid.yml数据库配置文件中更改数据库地址
3.  执行sql文件夹中的sql
4.  如需调试微信小程序消息推送可更改com.expired.core.domain.WeChatPushDTO类的miniprogram_state字段在值
    
```
   /**
     * 跳转小程序类型：
     * developer为开发版；
     * trial为体验版；
     * formal为正式版；
     * 默认为正式版
     */
    private String miniprogram_state = "developer"; 
```
 
 **前端服务：**
- 1、在common/global.js文件修改后端接口实际地址
    const apiUrl = "http://127.0.0.1:9999/expired";
- 2、在common/global.js文件配置微信小程序消息模板id
    const tmplIds = ['过期消息推送模板id','到期消息推送模板id'];

#### 使用说明
![小程序码](https://foruda.gitee.com/images/1692263567122893552/a16596a4_8450933.jpeg "小程序码.jpg")

![登录](https://foruda.gitee.com/images/1692263598567504577/ddee9be5_8450933.png "屏幕截图")

![个人中心](https://foruda.gitee.com/images/1692263618388058802/9697b8fd_8450933.png "屏幕截图")

![首页](https://foruda.gitee.com/images/1692263633491447059/0a3173b4_8450933.png "屏幕截图")

![标签页](https://foruda.gitee.com/images/1692263648435536200/350f4fd8_8450933.png "屏幕截图")

![添加页](https://foruda.gitee.com/images/1692263668298077862/fbcfbb2e_8450933.png "屏幕截图")

![推送授权](https://foruda.gitee.com/images/1692263710275685328/cfc2fe76_8450933.png "屏幕截图")

![消息推送](https://foruda.gitee.com/images/1692263724997176085/8f08ec84_8450933.png "屏幕截图")

#### 参与贡献

- 1. 基于若依框架搭建改造
- 2. 前后端包括页面设计都由博主开发完成

### 最后
感谢点个 :star: 个人博客地址：https://blog.csdn.net/weixin_43566316?spm=1000.2115.3001.5343

