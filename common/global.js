const apiUrl = "http://127.0.0.1:9999/expired";
const tabList = [{
		iconPath: "/static/tarbarIcon/首页.png",
		selectedIconPath: "/static/tarbarIcon/home-active.png",
		text: '首页',
		pagePath: "/pages/index/home",
		customIcon: false,
	},
	{
		iconPath: "/static/tarbarIcon/添加.png",
		selectedIconPath: "/static/tarbarIcon/add-active.png",
		text: '添加',
		pagePath: "/pages/index/addGoods",
		midButton: true,
		customIcon: false,
	},
	{
		iconPath: "/static/tarbarIcon/个人中心.png",
		selectedIconPath: "/static/tarbarIcon/user-active.png",
		pagePath: "/pages/index/myCenter",
		text: '我的',
		customIcon: false,
	},
];
const tmplIds = ['过期消息推送模板id','到期消息推送模板id'];

export default {
	apiUrl,
	tabList,
	tmplIds
}