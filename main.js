
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
import global from '@/common/global.js';
Vue.use(uView);
Vue.prototype.global = global
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
// http拦截器
import httpInterceptor from '@/common/http.interceptor.js'
Vue.use(httpInterceptor, app)
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif